Fotorama Drupal Module
======================

1. How to install Fotorama



1. How to install Fotorama
--------------------------

1. Make sure the Libraries module is installed.
   You can download it from it's project page at
   http://drupal.org/project/libraries

2. Download the Fotorama from
   http://fotoramajs.com/download/
   and extract it to "fotorama" folder into your /sites/all/libraries/ directory.

3. Extract the Fotorama Drupal module into your /sites/all/modules/ directory and
   enable it (module "Fotorama" in category "Other").
